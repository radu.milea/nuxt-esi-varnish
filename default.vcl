vcl 4.1;

backend default {
  .host = "host.docker.internal";
  .port = "3000";
}

sub vcl_backend_response {
  set beresp.do_esi = true;
  set beresp.ttl = 0s;
    # if (bereq.url == "/article") {
    #    set beresp.do_esi = false;
    # } else {
    #   set beresp.do_esi = true; 
    #   set beresp.ttl = 30s;
    # }
}
